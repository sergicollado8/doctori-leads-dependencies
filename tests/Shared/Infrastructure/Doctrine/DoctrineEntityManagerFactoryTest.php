<?php

declare(strict_types=1);

namespace DoctorI\Autos\Tests\Shared\Infrastructure\Doctrine;

use DoctorI\Autos\Shared\Infrastructure\Doctrine\DoctrineEntityManagerFactory;
use DoctorI\Autos\Shared\Infrastructure\Doctrine\LeadEntityManagerFactory;
use PHPUnit\Framework\TestCase;

class DoctrineEntityManagerFactoryTest extends TestCase
{

    public function testCreate(): void
    {
        $params =
            [
                'driver' => 'pdo_mysql',
                'host' => 'autos-leads-dependencies-mysql',
                'port' => '3306',
                'dbname' => 'autos_leads',
                'user' => 'root',
                'password' => 'AutosLeads2020!',
            ];
        LeadEntityManagerFactory::create($params, 'dev');
    }
}
