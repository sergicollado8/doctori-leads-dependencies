<?php

declare(strict_types=1);

namespace DoctorI\Autos\Tests\Shared\Infrastructure\Bus\Event\RabbitMq;

use AMQPConnection;
use DoctorI\Autos\Shared\Infrastructure\Bus\Event\RabbitMq\RabbitMqConnection;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RabbitMqConnectionTest extends KernelTestCase
{
    private array $configuration;
    private static ?AMQPConnection $connection = null;

    protected function setUp(): void
    {
        $this->configuration = [
            'host' => 'autos-leads-dependencies-rabbitmq',
            'port' => '5672',
            'vhost' => '/',
            'login' => 'doctori',
            'password' => 'doctori',
            'read_timeout' => 2,
            'write_timeout' => 2,
            'connect_timeout' => 5,
        ];
        parent::setUp();
    }


    public function testConnection()
    {
        if (null === self::$connection) {
//            self::$connection = self::$container->get(RabbitMqConnection::class);
            self::$connection = new AMQPConnection($this->configuration);
            print_r('hola');
        }

        if (!self::$connection->isConnected()) {
            print_r('hola1');
            self::$connection->pconnect();
        }
        print_r('hola2');

        return self::$connection;
    }
}
