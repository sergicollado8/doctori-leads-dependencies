<?php

declare(strict_types=1);

namespace DoctorI\Autos\Client\Infrastructure\Persistence;

use DoctorI\Autos\Client\Domain\Client;
use DoctorI\Autos\Client\Domain\ClientRepository;
use DoctorI\Shared\Doctrine\Infrastructure\Persistence\Doctrine\DoctrineRepository;

class ClientRepositoryMySQL extends DoctrineRepository implements ClientRepository
{
    public function save(Client $client): void
    {
        $this->persist($client);
    }
}
