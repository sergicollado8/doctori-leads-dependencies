<?php

declare(strict_types=1);

namespace DoctorI\Autos\Client\Domain;

use DoctorI\Autos\Shared\Domain\Aggregate\AggregateRoot;

final class Client extends AggregateRoot
{
    public function __construct(
        private ClientId $id,
        private ClientName $name,
        private ClientLeadId $leadId
    ) {
    }

    public static function create(
        ClientId $id,
        ClientName $name,
        ClientLeadId $leadId
    ): self {
        $client = new self($id, $name, $leadId);
//        $lead->record(new LeadCreateDomainEvent($id->value(), $name->value(), $email->value(), $phone->value()));
        return $client;
    }

    public function id(): ClientId
    {
        return $this->id;
    }

    public function name(): ClientName
    {
        return $this->name;
    }

}
