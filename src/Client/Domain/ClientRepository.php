<?php

declare(strict_types=1);

namespace DoctorI\Autos\Client\Domain;

interface ClientRepository
{
    public function save(Client $client): void;
}
