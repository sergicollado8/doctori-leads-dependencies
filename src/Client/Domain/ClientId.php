<?php

declare(strict_types=1);

namespace DoctorI\Autos\Client\Domain;

use DoctorI\Shared\Domain\ValueObject\Uuid;

final class ClientId extends Uuid
{

}
