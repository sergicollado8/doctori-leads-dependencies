<?php

declare(strict_types=1);

namespace DoctorI\Autos\Client\Application\Create;

use DoctorI\Autos\Client\Domain\Client;
use DoctorI\Autos\Client\Domain\ClientId;
use DoctorI\Autos\Client\Domain\ClientLeadId;
use DoctorI\Autos\Client\Domain\ClientName;
use DoctorI\Autos\Client\Domain\ClientRepository;

final class ClientCreator
{
    public function __construct(private ClientRepository $repository)
    {
    }

    public function create(string $id, string $name, string $leadId): void
    {
        $this->repository->save(new Client(new ClientId($id), new ClientName($name), new ClientLeadId($leadId)));
    }
}
