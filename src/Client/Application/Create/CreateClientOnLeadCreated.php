<?php

declare(strict_types=1);

namespace DoctorI\Autos\Client\Application\Create;

use DoctorI\Autos\Lead\Domain\LeadCreateDomainEvent;
use DoctorI\Shared\Domain\ValueObject\Uuid;
use DoctorI\Shared\EventBus\Domain\Bus\Event\DomainEventSubscriber;

final class CreateClientOnLeadCreated implements DomainEventSubscriber
{
    public function __construct(private ClientCreator $creator)
    {
    }

    public static function subscribedTo(): array
    {
        return [LeadCreateDomainEvent::class];
    }

    public function __invoke(LeadCreateDomainEvent $event): void
    {
        $this->creator->create(Uuid::random()->value(), $event->name(), $event->aggregateId());
    }
}
