<?php

declare(strict_types=1);

namespace DoctorI\Autos\Shared\Domain\Aggregate;

use DoctorI\Shared\EventBus\Domain\Bus\Event\DomainEvent;
use DoctorI\Shared\Domain\ValueObject\Aggregate\AggregateRootBase;

abstract class AggregateRoot extends AggregateRootBase
{
    private array $domainEvents = [];

    final public function pullDomainEvents(): array
    {
        $domainEvents       = $this->domainEvents;
        $this->domainEvents = [];

        return $domainEvents;
    }

    final protected function record(DomainEvent $domainEvent): void
    {
        $this->domainEvents[] = $domainEvent;
    }
}
