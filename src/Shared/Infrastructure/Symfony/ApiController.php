<?php

declare(strict_types=1);

namespace DoctorI\Autos\Shared\Infrastructure\Symfony;

use DoctorI\Shared\CommandQueryBus\Domain\Bus\Command\Command;
use DoctorI\Shared\CommandQueryBus\Domain\Bus\Command\CommandBus;
use DoctorI\Shared\CommandQueryBus\Domain\Bus\Query\Query;
use DoctorI\Shared\CommandQueryBus\Domain\Bus\Query\QueryBus;
use DoctorI\Shared\CommandQueryBus\Domain\Bus\Query\Response;

use function Lambdish\Phunctional\each;

abstract class ApiController
{
    public function __construct(
        private QueryBus $queryBus,
        private CommandBus $commandBus,
        ApiExceptionsHttpStatusCodeMapping $exceptionHandler
    ) {
        each(
            fn(int $httpCode, string $exceptionClass) => $exceptionHandler->register($exceptionClass, $httpCode),
            $this->exceptions()
        );
    }

    abstract protected function exceptions(): array;

    protected function ask(Query $query): ?Response
    {
        return $this->queryBus->ask($query);
    }

    protected function dispatch(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
