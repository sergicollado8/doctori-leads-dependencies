<?php

declare(strict_types=1);

namespace DoctorI\Autos\Shared\Infrastructure\Doctrine;

use DoctorI\Shared\Doctrine\Infrastructure\Doctrine\DoctrineEntityManagerFactory;
use Doctrine\ORM\EntityManagerInterface;

final class LeadEntityManagerFactory
{
    private const SCHEMA_PATH = __DIR__ . '/../../../../etc/databases/lead.sql';
    private static array $sharedPrefixes = [
        __DIR__ . '/../../../Lead/Infrastructure/Persistence/Doctrine' => 'DoctorI\Autos\Lead\Domain',
        __DIR__ . '/../../../Client/Infrastructure/Persistence/Doctrine' => 'DoctorI\Autos\Client\Domain',
        __DIR__ . '/../../../Offer/Infrastructure/Persistence/Doctrine' => 'DoctorI\Autos\Offer\Domain',
    ];

    public static function create(array $parameters, string $environment): EntityManagerInterface
    {
        $isDevMode = 'prod' !== $environment;

        $prefixes = array_merge(
            DoctrinePrefixesSearcher::inPath(__DIR__ . '/../../../Lead', 'DoctorI\Autos\Lead'),
            DoctrinePrefixesSearcher::inPath(__DIR__ . '/../../../Client', 'DoctorI\Autos\Client'),
            DoctrinePrefixesSearcher::inPath(__DIR__ . '/../../../Offer', 'DoctorI\Autos\Offer'),
        );

        $dbalCustomTypesClasses = DbalTypesSearcher::inPath(__DIR__ . '/../../../Lead', 'Lead');

        return DoctrineEntityManagerFactory::create(
            $parameters,
            $prefixes,
            $isDevMode,
            self::SCHEMA_PATH,
            $dbalCustomTypesClasses,
            self::$sharedPrefixes
        );
    }
}
