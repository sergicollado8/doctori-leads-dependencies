<?php

declare(strict_types=1);

namespace DoctorI\Autos\Controller\Lead;

use DoctorI\Autos\Lead\Domain\Lead;
use DoctorI\Autos\Lead\Domain\LeadEmail;
use DoctorI\Autos\Lead\Domain\LeadId;
use DoctorI\Autos\Lead\Domain\LeadName;
use DoctorI\Autos\Lead\Domain\LeadPhone;
use DoctorI\Autos\Shared\Infrastructure\Symfony\ApiController;
use DoctorI\Shared\Domain\ValueObject\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;

final class LeadGetController extends ApiController
{
    public function __invoke(string $id): JsonResponse
    {
//        $response = $this->ask(new LeadFinder())
//        $response = $this->queryBus->ask(
//            new LeadFinder()
//        )
//        $response = new LeadFinder()
        $lead = new Lead(
            new LeadId(Uuid::random()->value()),
            new LeadName('sergi'),
            new LeadEmail('sergicollado@isalud.com'),
            new LeadPhone('66666666')
        );
//        return new JsonResponse($lead);
        return new JsonResponse(
            [
                'id' => $lead->id()->value(),
                'name' => $lead->name()->value(),
                'email' => $lead->phone()->value(),
                'phone' => $lead->email()->value()
            ]
        );
        // TODO: Implement __invoke() method.
    }

    protected function exceptions(): array
    {
        return [];
    }
}
