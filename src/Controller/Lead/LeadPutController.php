<?php

declare(strict_types=1);

namespace DoctorI\Autos\Controller\Lead;

use DoctorI\Autos\Lead\Application\Create\CreateLeadCommand;
use DoctorI\Autos\Shared\Infrastructure\Symfony\ApiController;
use DoctorI\Shared\Domain\ValueObject\Exception\InvalidEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class LeadPutController extends ApiController
{
    public function __invoke(string $id, Request $request): Response
    {
        $this->dispatch(
            new CreateLeadCommand(
                $id,
                $request->request->get('name'),
                $request->request->get('phone'),
                $request->request->get('email'),
            )
        );
        return new Response('', Response::HTTP_CREATED);
    }

    protected function exceptions(): array
    {
        return [
            InvalidEmail::class => Response::HTTP_BAD_REQUEST,
        ];
    }
}
