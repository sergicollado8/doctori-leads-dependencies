<?php

declare(strict_types=1);

namespace DoctorI\Autos\Controller\Lead;

use DoctorI\Autos\Lead\Application\LeadResponse;
use DoctorI\Autos\Lead\Application\LeadsResponse;
use DoctorI\Autos\Lead\Application\SearchAll\SearchLeadAllQuery;
use DoctorI\Autos\Shared\Infrastructure\Symfony\ApiController;
use Symfony\Component\HttpFoundation\JsonResponse;

use function Lambdish\Phunctional\map;

final class LeadGetAllController extends ApiController
{
    public function __invoke(): JsonResponse
    {
        /** @var LeadsResponse $response */
        $response = $this->ask(new SearchLeadAllQuery());
        return new JsonResponse(
            map(
                fn(LeadResponse $lead) => [
                    'id' => $lead->id(),
                    'name' => $lead->name(),
                    'email' => $lead->email(),
                    'phone' => $lead->phone()
                ],
                $response->leads()
            )
        );
    }

    protected function exceptions(): array
    {
        return [];
    }
}
