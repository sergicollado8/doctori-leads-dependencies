<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Infrastructure\Persistence;

use DoctorI\Autos\Lead\Domain\Lead;
use DoctorI\Autos\Lead\Domain\LeadId;
use DoctorI\Autos\Lead\Domain\LeadRepository;
use DoctorI\Shared\Doctrine\Infrastructure\Persistence\Doctrine\DoctrineRepository;

class LeadRepositoryMySQL extends DoctrineRepository implements LeadRepository
{

    public function save(Lead $lead): void
    {
        $this->persist($lead);
    }

    public function search(LeadId $id): ?Lead
    {
        return $this->repository(Lead::class)->find($id);
    }

    public function searchAll(): array
    {
        return $this->repository(Lead::class)->findAll();
    }
}
