<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Domain;

interface LeadRepository
{
    public function save(Lead $lead): void;

    public function search(LeadId $id): ?Lead;

    public function searchAll();
}
