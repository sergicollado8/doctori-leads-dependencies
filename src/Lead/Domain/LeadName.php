<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Domain;

use DoctorI\Shared\Domain\ValueObject\Text;

final class LeadName extends Text
{
}
