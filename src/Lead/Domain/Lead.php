<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Domain;

use DoctorI\Autos\Shared\Domain\Aggregate\AggregateRoot;

final class Lead extends AggregateRoot
{
    public function __construct(
        private LeadId $id,
        private LeadName $name,
        private LeadEmail $email,
        private LeadPhone $phone
    ) {
    }

    public static function create(
        LeadId $id,
        LeadName $name,
        LeadEmail $email,
        LeadPhone $phone
    ): self {
        $lead = new self($id, $name, $email, $phone);
        $lead->record(new LeadCreateDomainEvent($id->value(), $name->value(), $email->value(), $phone->value()));
        return $lead;
    }

    public function id(): LeadId
    {
        return $this->id;
    }

    public function name(): LeadName
    {
        return $this->name;
    }

    public function email(): LeadEmail
    {
        return $this->email;
    }

    public function phone(): LeadPhone
    {
        return $this->phone;
    }
}
