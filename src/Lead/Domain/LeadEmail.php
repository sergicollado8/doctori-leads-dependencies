<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Domain;

use DoctorI\Shared\Domain\ValueObject\Email;

final class LeadEmail extends Email
{
}
