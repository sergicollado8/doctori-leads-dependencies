<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Domain;

use DoctorI\Shared\EventBus\Domain\Bus\Event\DomainEvent;

final class LeadCreateDomainEvent extends DomainEvent
{
    public function __construct(
        string $id,
        private string $name,
        private string $email,
        private string $phone,
        string $eventId = null,
        string $occurredOn = null
    ) {
        parent::__construct($id, $eventId, $occurredOn);
    }

    public static function fromPrimitives(
        string $aggregateId,
        array $body,
        string $eventId,
        string $occurredOn
    ): DomainEvent {
        return new self($aggregateId, $body['name'], $body['email'], $body['phone'], $eventId, $occurredOn);
    }

    public static function eventName(): string
    {
        return 'lead.created';
    }

    public function toPrimitives(): array
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
        ];
    }

    public function name(): string
    {
        return $this->name;
    }
}
