<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Domain;

use DoctorI\Shared\Domain\ValueObject\Uuid;

final class LeadId extends Uuid
{
}
