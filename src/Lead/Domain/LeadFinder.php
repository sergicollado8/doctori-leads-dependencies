<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Domain;

use RuntimeException;

final class LeadFinder
{
    public function __construct(private LeadRepository $repository)
    {
    }

    public function __invoke(LeadId $id): Lead
    {
        $lead = $this->repository->search($id);

        if (null === $lead) {
            throw new RuntimeException('Lead not exist.');
        }

        return $lead;
    }
}
