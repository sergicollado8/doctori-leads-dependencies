<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Application;

//use DoctorI\Autos\Shared\Domain\Bus\Query\Response;

use DoctorI\Shared\CommandQueryBus\Domain\Bus\Query\Response;

final class LeadsResponse implements Response
{
    private array $leads;

    public function __construct(LeadResponse ...$leads)
    {
        $this->leads = $leads;
    }

    public function leads(): array
    {
        return $this->leads;
    }
}
