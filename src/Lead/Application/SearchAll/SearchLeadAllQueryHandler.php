<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Application\SearchAll;

use DoctorI\Autos\Lead\Application\LeadsResponse;
use DoctorI\Shared\CommandQueryBus\Domain\Bus\Query\QueryHandler;

//use DoctorI\Autos\Shared\Domain\Bus\Query\QueryHandler;

final class SearchLeadAllQueryHandler implements QueryHandler
{
    public function __construct(private LeadAllSearcher $searcher)
    {
    }

    public function __invoke(SearchLeadAllQuery $query): LeadsResponse
    {
        return $this->searcher->searchAll();
    }
}
