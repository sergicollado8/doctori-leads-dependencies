<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Application\SearchAll;

use DoctorI\Autos\Lead\Application\LeadResponse;
use DoctorI\Autos\Lead\Application\LeadsResponse;
use DoctorI\Autos\Lead\Domain\Lead;
use DoctorI\Autos\Lead\Domain\LeadRepository;

use function Lambdish\Phunctional\map;

final class LeadAllSearcher
{
    public function __construct(private LeadRepository $repository)
    {
    }

    public function searchAll(): LeadsResponse
    {
        return new LeadsResponse(...map($this->response(), $this->repository->searchAll()));
    }

    private function response(): callable
    {
        return static fn(Lead $lead) => new LeadResponse(
            $lead->id()->value(), $lead->name()->value(), $lead->email()->value(), $lead->phone()->value()
        );
    }
}
