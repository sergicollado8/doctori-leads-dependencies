<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Application;

final class LeadResponse
{
    public function __construct(private string $id, private string $name, private string $email, private string $phone)
    {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function phone(): string
    {
        return $this->phone;
    }
}
