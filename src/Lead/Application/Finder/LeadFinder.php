<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Application\Finder;

use DoctorI\Autos\Lead\Domain\Lead;
use DoctorI\Autos\Lead\Domain\LeadFinder as DomainLeadFinder;
use DoctorI\Autos\Lead\Domain\LeadId;
use DoctorI\Autos\Lead\Domain\LeadRepository;

final class LeadFinder
{
    private DomainLeadFinder $finder;

    public function __construct(LeadRepository $repository)
    {
        $this->finder = new DomainLeadFinder($repository);
    }

    public function __invoke(LeadId $id): Lead
    {
        return $this->finder->__invoke($id);
    }
}
