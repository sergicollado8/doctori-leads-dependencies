<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Application\Create;

use DoctorI\Autos\Lead\Domain\LeadEmail;
use DoctorI\Autos\Lead\Domain\LeadId;
use DoctorI\Autos\Lead\Domain\LeadName;
use DoctorI\Autos\Lead\Domain\LeadPhone;
use DoctorI\Shared\CommandQueryBus\Domain\Bus\Command\CommandHandler;

//use DoctorI\Autos\Shared\Domain\Bus\Command\CommandHandler;

final class CreateLeadCommandHandler implements CommandHandler
{
    public function __construct(private LeadCreator $creator)
    {
    }

    public function __invoke(CreateLeadCommand $command): void
    {
        $id = new LeadId($command->id());
        $name = new LeadName($command->name());
        $email = new LeadEmail($command->email());
        $phone = new LeadPhone($command->phone());

        $this->creator->__invoke($id, $name, $email, $phone);
    }
}
