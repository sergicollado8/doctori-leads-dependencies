<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Application\Create;

use DoctorI\Autos\Lead\Domain\Lead;
use DoctorI\Autos\Lead\Domain\LeadEmail;
use DoctorI\Autos\Lead\Domain\LeadId;
use DoctorI\Autos\Lead\Domain\LeadName;
use DoctorI\Autos\Lead\Domain\LeadPhone;
use DoctorI\Autos\Lead\Domain\LeadRepository;
use DoctorI\Shared\EventBus\Domain\Bus\Event\EventBus;

final class LeadCreator
{
    public function __construct(private LeadRepository $repository, private EventBus $bus)
    {
    }

    public function __invoke(LeadId $id, LeadName $name, LeadEmail $email, LeadPhone $phone)
    {

        $lead = Lead::create($id, $name, $email, $phone);

        $this->repository->save($lead);
        $this->bus->publish(...$lead->pullDomainEvents());
    }
}
