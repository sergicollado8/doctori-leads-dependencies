<?php

declare(strict_types=1);

namespace DoctorI\Autos\Lead\Application\Create;

//use DoctorI\Autos\Shared\Domain\Bus\Command\Command;

use DoctorI\Shared\CommandQueryBus\Domain\Bus\Command\Command;

final class CreateLeadCommand implements Command
{
    public function __construct(private string $id, private string $name, private string $phone, private string $email)
    {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function phone(): string
    {
        return $this->phone;
    }

    public function email(): string
    {
        return $this->email;
    }
}
