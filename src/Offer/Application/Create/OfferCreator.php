<?php

declare(strict_types=1);

namespace DoctorI\Autos\Offer\Application\Create;

use DoctorI\Autos\Offer\Domain\Offer;
use DoctorI\Autos\Offer\Domain\OfferId;
use DoctorI\Autos\Offer\Domain\OfferLeadId;
use DoctorI\Autos\Offer\Domain\ClientName;
use DoctorI\Autos\Offer\Domain\OfferRepository;

final class OfferCreator
{
    public function __construct(private OfferRepository $repository)
    {
    }

    public function create(string $id, string $leadId): void
    {
        $this->repository->save(new Offer(new OfferId($id), new OfferLeadId($leadId)));
    }
}
