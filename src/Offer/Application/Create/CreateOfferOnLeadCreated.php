<?php

declare(strict_types=1);

namespace DoctorI\Autos\Offer\Application\Create;

use DoctorI\Autos\Lead\Domain\LeadCreateDomainEvent;
use DoctorI\Shared\EventBus\Domain\Bus\Event\DomainEventSubscriber;
use DoctorI\Shared\Domain\ValueObject\Uuid;

final class CreateOfferOnLeadCreated implements DomainEventSubscriber
{
    public function __construct(private OfferCreator $creator)
    {
    }

    public static function subscribedTo(): array
    {
        return [LeadCreateDomainEvent::class];
    }

    public function __invoke(LeadCreateDomainEvent $event): void
    {
        $this->creator->create(Uuid::random()->value(), $event->aggregateId());
    }
}
