<?php

declare(strict_types=1);

namespace DoctorI\Autos\Offer\Infrastructure\Persistence;

use DoctorI\Autos\Offer\Domain\Offer;
use DoctorI\Autos\Offer\Domain\OfferRepository;
use DoctorI\Shared\Doctrine\Infrastructure\Persistence\Doctrine\DoctrineRepository;

class OfferRepositoryMySQL extends DoctrineRepository implements OfferRepository
{
    public function save(Offer $offer): void
    {
        $this->persist($offer);
    }
}
