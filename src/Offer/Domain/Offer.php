<?php

declare(strict_types=1);

namespace DoctorI\Autos\Offer\Domain;

use DoctorI\Autos\Shared\Domain\Aggregate\AggregateRoot;

final class Offer extends AggregateRoot
{
    public function __construct(
        private OfferId $id,
        private OfferLeadId $leadId
    ) {
    }

    public static function create(
        OfferId $id,
        OfferLeadId $leadId
    ): self {
        $offer = new self($id, $leadId);
//        $lead->record(new LeadCreateDomainEvent($id->value(), $name->value(), $email->value(), $phone->value()));
        return $offer;
    }

    public function id(): OfferId
    {
        return $this->id;
    }

}
