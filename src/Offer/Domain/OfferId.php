<?php

declare(strict_types=1);

namespace DoctorI\Autos\Offer\Domain;

use DoctorI\Shared\Domain\ValueObject\Uuid;

final class OfferId extends Uuid
{

}
