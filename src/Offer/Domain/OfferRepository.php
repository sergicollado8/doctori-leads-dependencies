<?php

declare(strict_types=1);

namespace DoctorI\Autos\Offer\Domain;

interface OfferRepository
{
    public function save(Offer $offer): void;
}
