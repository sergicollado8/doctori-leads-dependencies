create table `leads`
(
    id         varchar(36)             not null,
    name       varchar(50)             not null,
    email      varchar(100)            not null,
    phone      varchar(10)             not null,
    created_at timestamp default now() not null,
    constraint leads_id_uindex
        unique (id)
);

alter table `leads`
    add primary key (id);

create table `clients`
(
    id         varchar(36)             not null,
    name       varchar(50)             not null,
    lead_id    varchar(36)             not null,
    created_at timestamp default now() not null,
    constraint clients_id_uindex
        unique (id)
);

alter table `clients`
    add primary key (id);


create table `offers`
(
    id         varchar(36)             not null,
    lead_id    varchar(36)             not null,
    created_at timestamp default now() not null,
    constraint offers_id_uindex
        unique (id)
);

alter table `offers`
    add primary key (id);

CREATE TABLE `domain_events`
(
    `id`           CHAR(36)     NOT NULL,
    `aggregate_id` CHAR(36)     NOT NULL,
    `name`         VARCHAR(255) NOT NULL,
    `body`         JSON         NOT NULL,
    `occurred_on`  timestamp    NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
