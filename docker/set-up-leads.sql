create table `leads`
(
    uuid  varchar(36)  not null,
    name  varchar(50)  not null,
    email varchar(100) not null,
    phone varchar(10)  not null,
    constraint lead_uuid_uindex
        unique (uuid)
);

alter table `leads`
    add primary key (uuid);
