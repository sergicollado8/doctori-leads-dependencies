current-dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

.PHONY: build
build: deps start

.PHONY: deps
deps: composer-install

# 🐘 Composer
composer-env-file:
	@if [ ! -f .env.local ]; then echo '' > .env.local; fi

.PHONY: composer-install
composer-install: CMD=install

.PHONY: composer-update
composer-update: CMD=update

.PHONY: composer-require
composer-require: CMD=require
composer-require: INTERACTIVE=-ti --interactive

.PHONY: composer-require-module
composer-require-module: CMD=require $(module)
composer-require-module: INTERACTIVE=-ti --interactive

.PHONY: composer
composer composer-install composer-update composer-require composer-require-module: composer-env-file
	@docker run --rm $(INTERACTIVE) --volume $(current-dir):/app --user $(id -u):$(id -g) \
		composer:2 $(CMD) \
			--ignore-platform-reqs \
			--no-ansi

# 🐳 Docker Compose
.PHONY: start
start: CMD=up -d

.PHONY: stop
stop: CMD=stop

.PHONY: destroy
destroy: CMD=down

# Usage: `make doco CMD="ps --services"`
# Usage: `make doco CMD="build --parallel --pull --force-rm --no-cache"`
.PHONY: doco
doco start stop destroy: composer-env-file
	@docker-compose $(CMD)

.PHONY: rebuild
rebuild: composer-env-file
	docker-compose build --pull --force-rm --no-cache
	make deps
	make start

.PHONY: create-db
create-db:
	docker exec autos-leads-dependencies-mysql /bin/sh -c 'mysql --user=root --password=AutosLeads2020! leads < set-up-leads.sql'

.PHONY: clean-cache
clean-cache:
	@rm -rf apps/*/*/var
	@docker exec autos-leads-dependencies-php ./bin/console cache:warmup

.PHONY: create-entity
create-entity:
	@mkdir ./src/$(filter-out $@,$(MAKECMDGOALS))
	@mkdir ./src/$(filter-out $@,$(MAKECMDGOALS))/Application
	@mkdir ./src/$(filter-out $@,$(MAKECMDGOALS))/Domain
	@mkdir ./src/$(filter-out $@,$(MAKECMDGOALS))/Infrastructure
	@mkdir ./src/$(filter-out $@,$(MAKECMDGOALS))/Infrastructure/Persistence
	@mkdir ./src/$(filter-out $@,$(MAKECMDGOALS))/Infrastructure/Persistence/Doctrine

.PHONY: check-style
check-style:
	./vendor/bin/phpcs --standard=PSR12 src tests --runtime-set ignore_errors_on_exit 1 --runtime-set ignore_warnings_on_exit 1

.PHONY: fix-style
fix-style:
	./vendor/bin/phpcbf --standard=PSR12 src tests

.PHONY: inspect-phpstan
inspect-phpstan:
	./vendor/bin/phpstan analyse -l 4 src tests
